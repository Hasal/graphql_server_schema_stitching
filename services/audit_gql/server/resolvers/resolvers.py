import logging
import os
from typing import Text

# import psutil
# from dotenv import load_dotenv
from graphene import Field, ObjectType, Schema, String
from graphql import GraphQLError
from server.database import init_db
from server.schema.health import Health
from server.schema.main_question import MainQuestion
from server.schema.project import Project
from server.schema.sub_question import SubQuestion

logger = logging.getLogger(__name__)
graph = init_db()
port = os.environ.get('PORT')


class Query(ObjectType):

    # Defines a health resolver
    health = Field(Health)

    # Defines a query which returns the project overview
    project_overview = Field(
        Project,
        project_name=String(required=True)
    )

    # Defines a query which returns the main questions
    main_question = Field(
        MainQuestion,
        id=String(required=True)
    )

    # Defines a query which returns the sub questions
    sub_question = Field(
        SubQuestion,
        id=String(required=True)
    )

    # Defines a query which returns the next question
    next_question = Field(
        MainQuestion,
        project_name=String(required=True)
    )

    """
    @staticmethod
    def resolve_health(root, info) -> Health:

        returns the health status of the GraphQL API
        and other critical services


        # use psutil to check the connection of graphql server/server is running
        connections_list = psutil.net_connections()
        if any(connection.laddr.port == port and connection.laddr.ip is not None and
                connection.status == 'ESTABLISHED' for connection in connections_list):
            graphql_status = 'OK'
        else:
            graphql_status = 'NOT OK'

        cypher_result = graph.run(
            "MATCH (n:MainQuestion{id:'1'}) RETURN n.id"
        ).data()
        cypher_result = cypher_result[0]['n.id']                   # with graphene.Field
        # cypher_result = [row['n'] for row in cypher_result]      # with graphene.List

        if (cypher_result == '1'):
            neo4j_status = 'OK'
        else:
            neo4j_status = 'NOT OK'

        services_status = {
                "services": [
                    {
                        "name": "Neo4j",
                        "status": neo4j_status
                    },
                    {
                        "name": "GraphQL",
                        "status": graphql_status
                    }
                ]
        }

        return services_status
        """

    @staticmethod
    def resolve_main_question(
            root,
            info,
            id: Text) -> MainQuestion:
        """
        retrieves the main question for given id
        """
        cypher_result = graph.run(
            "MATCH (n:MainQuestion{qid:"+repr(id)+"}) RETURN DISTINCT n"
        ).data()

        if len(cypher_result) > 0:
            cypher_result = cypher_result[0]['n']                      # with graphene.Field
            # cypher_result = [row['n'] for row in cypher_result]      # with graphene.List
        else:
            raise GraphQLError("question id is not correct")

        return cypher_result

    @staticmethod
    def resolve_sub_question(
            root,
            info,
            id: Text) -> SubQuestion:
        """
        retrieves the sub question for given id
        """
        cypher_result = graph.run(
            "MATCH (n:SubQuestion{qid:"+repr(id)+"}) RETURN DISTINCT n"
        ).data()

        if len(cypher_result) > 0:
            cypher_result = cypher_result[0]['n']                      # with graphene.Field
            # cypher_result = [row['n'] for row in cypher_result]      # with graphene.List
        else:
            raise GraphQLError("question id is not correct")

        return cypher_result

    @staticmethod
    def resolve_project_overview(
            root,
            info,
            project_name: Text) -> Project:
        """
        retrieves the project overview
        """
        valid_project_names = ['Production Line Audit 2023', '2022']
        if (project_name not in valid_project_names):
            raise GraphQLError(
                "project name:" + repr(project_name) +
                "is not valid,  name should be:" + repr(valid_project_names[0]) + " or "
                + repr(valid_project_names[1])
            )

        cypher_result = graph.run(
            "MATCH (n:Project{name:"+repr(project_name)+"}) RETURN DISTINCT n"
        ).data()

        if len(cypher_result) > 0:
            cypher_result2 = graph.run(
                "MATCH (n:Project{name:"+repr(project_name)+"})-[:HAS_COLLECTED_ANSWER]->"
                "(c:CollectedAnswer) RETURN DISTINCT c"
            ).data()
            if len(cypher_result2) > 0:
                collected_ans = [row['c'] for row in cypher_result2]
                cypher_result[0]['n']['collected_answers'] = collected_ans
            cypher_result = cypher_result[0]['n']

        return cypher_result

    @staticmethod
    def resolve_next_question(
            root,
            info,
            project_name: Text) -> MainQuestion:
        """
        retrieves the project overview
        """
        valid_project_names = ['Production Line Audit 2023', '2022']
        if (project_name not in valid_project_names):
            raise GraphQLError(
                "project name:" + repr(project_name) +
                "is not valid,  name should be:" + repr(valid_project_names[0]) + " or "
                + repr(valid_project_names[1])
            )

        cypher_result1 = graph.run(
            "MATCH (n:Project{name:"+repr(project_name)+"})-[:ACTIVE_QUESTION]->"
            "(q:MainQuestion) RETURN DISTINCT q"
        ).data()

        cypher_result2 = graph.run(
            "MATCH (n:Project{name:"+repr(project_name)+"})-[:ACTIVE_QUESTION]->"
            "(q:SubQuestion) RETURN DISTINCT q"
        ).data()

        if len(cypher_result1) > 0:
            cypher_result = cypher_result1[0]['q']
        elif len(cypher_result2) > 0:
            cypher_result = cypher_result2[0]['q']
        else:
            cypher_result = cypher_result1

        return cypher_result


schema = Schema(query=Query)
