# GraphQl module to build Collected Answer lists in the GraphQL schema

from graphene import ObjectType, String


class CollectedAnswer(ObjectType):
    """
    A class to represent the Collected Answer object type in graphql schema.

    Attributes
        question_node_id (str): id of the question node
        answer(str): answers are available or not,
        evidence(str): available evidences,
    """

    question_node_id = String(required=False)
    answer = String(required=False)
    evidence = String(required=False)
