# GraphQl module to build health of services in the GraphQL schema

from graphene import ObjectType, String


class Services(ObjectType):
    """
    A class to represent the health services object type in graphql schema.

    Attributes
        name (str): name of the service
        status (str): service status
    """

    name = String(required=False)
    status = String(required=False)
