# GraphQl module to build Task lists in the GraphQL schema

from graphene import ObjectType
from graphene import String


class Task(ObjectType):
    """
    A class to represent the defect object type in graphql schema.

    Attributes
        description (str): description of the task
    """

    description = String(required=False, name='description')
