# GraphQl module to build health of services in the GraphQL schema

from graphene import ObjectType, List as GList
from server.schema.services import Services


class Health(ObjectType):
    """
    A class to represent the Health object type in graphql schema.

    Attributes
        services (List): the available services
    """

    services = GList(Services, required=False)
