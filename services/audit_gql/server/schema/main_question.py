# GraphQl module to build Main Questions lists in the GraphQL schema

from graphene import ObjectType
from graphene import String


class MainQuestion(ObjectType):
    """
    A class to represent the Main Questions object type in graphql schema.

    Attributes
        qid (str): question id
        description(str): question
        question_node_id(str): node id
    """

    qid = String(required=False, name='id')
    description = String(required=False, name='description')
    question_node_id = String(required=False)
