# GraphQl module to build Project lists in the GraphQL schema

from graphene import Boolean, ObjectType, String, List as GList
from server.schema.collected_answer import CollectedAnswer


class Project(ObjectType):
    """
    A class to represent the Project object type in graphql schema.

    Attributes
        name (str): name of the project
        group_is_started(bool): started or not started,
        active_group_name(str): name of the active group,
        active_question_node_id(str): active question node id,
        pending_evidence(bool): evidences are there or not,
        collected_answers(list): a list of collected answers
    """

    name = String(required=False)
    group_is_started = Boolean(required=False)
    active_group_name = String(required=False)
    active_question_node_id = String(required=False)
    pending_evidence = Boolean(required=False)
    collected_answers = GList(CollectedAnswer, required=False)
