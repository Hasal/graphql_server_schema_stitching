import os

from py2neo import Graph


def init_db():

    uri = os.environ.get('DATABASE_URI')
    db = os.environ.get('DATABASE_NAME')
    pwd = os.environ.get('DATABASE_PWD')
    graph = Graph(uri, auth=(db, pwd))

    return graph
