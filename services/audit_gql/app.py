import os

import uvicorn
from fastapi import FastAPI
from starlette_graphene3 import GraphQLApp, make_graphiql_handler

from server.resolvers.resolvers import schema

# dotenv_path = join(dirname(__file__), '.env')
# load_dotenv(dotenv_path)
# load_dotenv('.env')

config_env = os.environ.get('CONFIG_ENV')

app = FastAPI()
app.add_route("/graphql", GraphQLApp(schema, on_get=make_graphiql_handler()))  # Graphiql IDE


if __name__ == "__main__":
    if config_env == "Development":
        config = uvicorn.Config(
            "__main__:app", host="0.0.0.0", port=4002, log_level="info",
            reload=True, access_log=True
        )
    else:
        config = uvicorn.Config(
            "__main__:app", host="0.0.0.0", port=4002, log_level="info",
            reload=False, access_log=True
        )
    server = uvicorn.Server(config)
    server.run()
