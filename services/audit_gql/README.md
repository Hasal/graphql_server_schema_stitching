
# GraphQL server using FastAPI, Py2neo and Graphene

This API is built on FastAPI Python Framework. This server API implements GraphQL query language using Graphene module and connects to a Neo4j graph database.

## Execution without docker
simply add the environment variable in the .env file and execute the app.py using ```$ python app.py```

## Installation and Execution using Docker

The docker-compose.yml file is included in the code which helps to set the configuration variables from .env file (which is also included) and runs the docker image in the container. Please execute the following steps. 
1. Configure the variable in the .env file accordingly.

2. Execute one of the following commands.

    `docker-compose up`
    or 
    `docker-compose up --force-recreate --build`
    or 
    `docker-compose up --force-recreate --build -d`

#### GraphQL Playground

Graphene provides an UI to execute the implemented Queries and Mutations in the server, visualize the input and return value types for those queries and mutations. **Also, the same query syntax is used in all consumable client languages.**

After the above installation and execution steps, Open a browser and go to 'localhost:portNumber/graphql'. The UI provides an input field to put quries or mutations, run them and see the results. Below are the inputs in GraphQL language implemented in this API.


#### Configuration variables

The environment variables will be picked automatically from .env file (included). Refer the below commands for options for the variables.

For Developemnt server

`CONFIG_ENV=Development`

For Production Serve (Not Ideal)

`CONFIG_ENV=Production`

Set the database URI and name

`DATABASE_URI=databaseuri`

`DATABASE_NAME=name`
`DATABASE_PWD=password`

If needed, set the port number(Default: localhost:4003)

`PORT=portnumber`

#### For gitlab runner
* Add these variables to the CI/CD Settings
```
$CONFIG_ENV  
$DATABASE_URI  
$DATABASE_NAME  
$DATABASE_PWD  
$PORT  
```
* Under build stage, put these scripts to the .gitlab-ci.yml
```
script:
    - sed -i "s/Development/$CONFIG_ENV/g" ./.env
    - sed -i "s/PUT_DATABSE_URI_HERE/$DATABASE_URI/g" ./.env
    - sed -i "s/PUT_DATABSE_NAME_HERE/$DATABASE_NAME/g" ./.env
    - sed -i "s/PUT_DATABSE_PWD_HERE/$DATABASE_PWD/g" ./.env
    - sed -i "s/4003/$PORT/g" ./.env

```
