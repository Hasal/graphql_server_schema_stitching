from typing import Any, Dict, List, Text
import yaml
import logging

log = logging.getLogger("Rasa config loader")


def get_service_names() -> List[Text]:
    '''Returns a list of service names from the endpoints.yaml file'''

    service_names = []

    with open("server/configs/endpoints.yaml", "r") as stream:
        try:

            for key in yaml.safe_load(stream).keys():
                if key != "version":
                    service_names.append(key)

            return service_names

        except yaml.YAMLError as exc:
            log.error(exc)


def get_credentials(service: Text) -> Dict[Text, Any]:
    ''' Returns the credentials for the service '''

    # log.debug(os.getcwd())
    # log.debug(os.listdir())

    with open("server/configs/credentials.yaml", "r") as stream:
        try:
            return yaml.safe_load(stream).get(service, {})
        except yaml.YAMLError as exc:
            log.error(exc)


def get_endpoint(service: Text) -> Dict[Text, Any]:
    ''' Returns the endpoint data for the service '''

    with open("server/configs/endpoints.yaml", "r") as stream:
        try:
            return yaml.safe_load(stream).get(service, {})
        except yaml.YAMLError as exc:
            log.error(exc)
