# This is a graphql resolvers file to build the GraphQL schema

# Standard library imports
import datetime
import logging
import numpy as np
import random
from typing import Any, Dict, List, Text

# Related third party imports
from graphene import Field, List as GList, ObjectType, Schema, String, Argument
from graphql import GraphQLError
import neomodel
import psutil

# from requests.adapters import HTTPAdapter
# from requests.packages.urllib3.util.retry import Retry

# helpers imports
# schema imports
from server.schema.action import Action, ActionType
from server.schema.characteristics import Characteristic, CharacteristicType
from server.schema.checklist import Checklist, ChecklistType
from server.schema.component import Component, ComponentType
from server.schema.defect import Defect, DefectType
from server.schema.defect_details import DefectDetailsType
from server.schema.health import HealthType
from server.schema.mutation_inputs import InputUserProductRel, InputProductStatus,\
                                          InputProductDefectRel, \
                                          InputProductProfile
from server.schema.objective import Objective, ObjectiveType
from server.schema.product_profile import ProductProfile, ProductProfileType
from server.schema.severity import Severity
from server.schema.status import Status
from server.schema.step import Step, StepType
from server.schema.user_profile import UserProfile
from server.schema.verb import Verb, VerbType

logger = logging.getLogger(__name__)


class Query(ObjectType):
    """
        A class to represent queries in graphql server and methods to represent
        resolvers.

    """
    # TODO : need to integrate improved error handeling

    '''
        General hints

        Queries can use variables:
        https://docs.graphene-python.org/en/latest/execution/execute/#variables
    '''

    # Defines a health resolver
    health = Field(HealthType)

    # Defines a query which returns the overview of the checklist
    checklist_overview = Field(
        ChecklistType,
        checklist_id=String(required=True),
        expertise_level=String(required=True)
    )

    # Defines a query which returns a list of steps
    checklist_steps = GList(
        StepType,
        checklist_id=String(required=True),
        checklist_step=String(default_value='not_given'),
        expertise_level=String(default_value='not_given')
    )

    # Defines a query which returns a list of characteristics
    checklist_characteristics = GList(
        CharacteristicType,
        checklist_id=String(required=True),
        checklist_step=String(default_value='not_given'),
        checklist_language=String(default_value='en'),
        defect_id=String(default_value='not_given'),
        severity=String(default_value='not_given')
    )

    # Defines a query which returns a list of DCS defects
    checklist_defects = GList(
        DefectType,
        checklist_id=String(required=True),
        checklist_step=String(default_value='not_given'),
        component=String(default_value='not_given'),
        severity=String(default_value='not_given'),
        characteristic=String(default_value='not_given')
    )

    # Defines a query which returns a list of components
    checklist_components = GList(
        ComponentType,
        checklist_id=String(required=True),
        checklist_step=String(default_value='not_given'),
        checklist_language=String(default_value='en'),
        defect_id=String(default_value='not_given')
    )

    # Defines a query which returns a list of actions
    checklist_actions = GList(
        ActionType,
        checklist_id=String(required=True),
        checklist_step=String(default_value='not_given'),
        checklist_language=String(default_value='en')
    )

    # Defines a query which returns a list of objectives
    checklist_objectives = GList(
        ObjectiveType,
        checklist_id=String(required=True),
        checklist_step=String(default_value='not_given'),
        checklist_language=String(default_value='en')
    )

    # Defines a query which returns a list of verbs
    checklist_verbs = GList(
        VerbType,
        checklist_id=String(required=True),
        checklist_step=String(default_value='not_given'),
        checklist_language=String(default_value='en')
    )

    # Defines a query which returns severity for a defect
    checklist_defect_details = Field(
        DefectDetailsType,
        checklist_id=String(required=True),
        checklist_language=String(default_value='en'),
        defect_id=String(required=True)
    )

    # Defines a query which returns a list of product profiles with status, scanned users, etc.
    product_profiles = GList(
        ProductProfileType,
        product_id=String(default_value='not_given')
    )

    @staticmethod
    def resolve_health(root, info) -> HealthType:
        """
        returns the health status of the GraphQL API
        and other critical services
        """

        # TODO : check the following graphql response
        # port = 4001
        # routing_parameter = 'graphql'
        # headers = {
        #        'Accept': 'application/json',
        #        'Content-Type': 'application/json'
        # }
        # url = f'http://graphql-server:{port}/{routing_parameter}/'
        # url = f'http://graphql-server:{port}/{routing_parameter}/#query=
        # %7BchecklistSteps(checklistId%3A"1")%20%7Bid%7D%7D&variables=&operationName=S'
        # session = requests.Session()
        # session.trust_env = False
        # retry = Retry(connect=1, backoff_factor=1)
        # adapter = HTTPAdapter(max_retries=retry)
        # session.mount('http://', adapter)
        # session.mount('https://', adapter)
        # graphql_response = session.get(url=url, headers=headers, timeout=3, verify=False)

        # use psutil to check the connection of graphql server/server is running
        connections_list = psutil.net_connections()
        if any(connection.laddr.port == 4001 and connection.laddr.ip is not None and
                connection.status == 'ESTABLISHED' for connection in connections_list):
            graphql_status = 'OK'
        else:
            graphql_status = 'NOT OK'

        cypher_query_result0, meta = neomodel.db.cypher_query(
                "MATCH (a :Checklist{checklist_id:'1'}) RETURN a")
        checklists = [Checklist.inflate(row[0]) for row in cypher_query_result0]

        if (checklists[0].checklist_id == '1'):
            neo4j_status = 'OK'
        else:
            neo4j_status = 'NOT OK'

        services_status = {
                "services": [
                    {
                        "name": "Neo4j",
                        "status": neo4j_status
                    },
                    {
                        "name": "GraphQL",
                        "status": graphql_status
                    }
                ]
        }

        return services_status

    @staticmethod
    def resolve_checklist_overview(
            root,
            info,
            checklist_id: Text,
            expertise_level: Text) -> ChecklistType:
        """
        retrieves the overview of a checklist belongs to a given expertise level
        """

        def step_sort(step) -> int:
            """inner method to sort the query step outputs"""
            sorted_step = int(step.step)
            return sorted_step

        expertise_level = expertise_level.capitalize()

        # validity of the checklist id
        cypher_query_result0, meta = neomodel.db.cypher_query(
                "MATCH (a :Checklist{checklist_id:"+repr(checklist_id)+"}) RETURN a")
        matched_checklists = [Checklist.inflate(row[0]) for row in cypher_query_result0]

        if (len(matched_checklists) == 0):

            # raise exception error
            raise GraphQLError(
                message="checklist id: "+repr(checklist_id)+" is not valid"
            )

        if ((expertise_level in
                ['expert', 'intermediate', 'novice', 'Expert', 'Intermediate', 'Novice'])):
            # a cypher query statement to retrieve data from the database
            cypher_query_result1, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(a:Step) WHERE a.type_always = ["+repr(expertise_level)+"] OR a.type_always ="
                " ["+repr(expertise_level)+", 'Intermediate'] OR a.type_always = "
                "['Expert', "+repr(expertise_level)+"] OR a.type_always = "
                "['Expert', 'Intermediate', 'Novice'] RETURN DISTINCT a"
            )
            steps_list = [Step.inflate(row[0]) for row in cypher_query_result1]
            steps_list = sorted(steps_list, key=step_sort)

            first_step = steps_list[0]
            last_step = steps_list[-1]

            checklist_overview = {
                'checklist_id': checklist_id,
                'expertise_level': expertise_level,
                'first_step': first_step,
                'last_step': last_step
            }

        if ((expertise_level not in
                ['expert', 'intermediate', 'novice', 'Expert', 'Intermediate', 'Novice']) and
                len(matched_checklists) != 0):

            # raise exception error
            raise GraphQLError(
                message="expertise_level: "+repr(expertise_level)+" is not valid"
            )

        if ((expertise_level not in
                ['expert', 'intermediate', 'novice', 'Expert', 'Intermediate', 'Novice']) and
                len(matched_checklists) == 0):

            # raise exception error
            raise GraphQLError(
                message="expertise_level: "+repr(expertise_level) +
                        " and checklist id: "+repr(checklist_id) + " are not valid",
            )

        return checklist_overview

    @staticmethod
    def resolve_checklist_steps(
            root,
            info,
            checklist_id: Text,
            checklist_step: Text,
            expertise_level: Text) -> List[StepType]:
        """
        retrieves the steps in a checklist, and the steps for a given expertise
        level in the checklist
        """
        expertise_level = expertise_level.capitalize()

        # uncomment this - to change random step selection precedure
        # seed = datetime.datetime.now().minute
        # seed_array = np.asarray([5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60])
        # idx = (np.abs(seed_array - seed)).argmin()
        # seed = seed_array[idx]
        # seed = idx+(datetime.datetime.now().day)*(datetime.datetime.now().hour)

        # A random step selection: changed with every day
        seed = datetime.datetime.now()
        seed = seed.strftime("%Y%m%d")
        seed = np.int_(seed)

        def step_sort(step) -> int:
            """inner method to sort the query step outputs"""
            sorted_step = int(step.step)
            return sorted_step

        if (checklist_step == 'not_given' and expertise_level == 'Not_given'):
            # a cypher query statement to retrieve data from the database
            cypher_query_result0, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(a:Step) RETURN DISTINCT a"
            )
            steps_list = [Step.inflate(row[0]) for row in cypher_query_result0]
            steps_list = sorted(steps_list, key=step_sort)
            first_step = steps_list[0]
            last_step = steps_list[-1]

            steps_list = [
                dict(item.__dict__, **{'first_step': first_step, 'last_step': last_step})
                for item in steps_list
            ]

        elif (checklist_step != 'not_given' and expertise_level == 'Not_given'):
            # a cypher query statement to retrieve data from the database
            cypher_query_result1, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(a:Step{step_id:"+repr(checklist_step)+"}) RETURN DISTINCT a"
            )
            steps_list = [Step.inflate(row[0]) for row in cypher_query_result1]

            # a cypher query statement to retrieve data from the database
            cypher_query_result2, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step{step_id:"+repr(checklist_step)+"})-[:HAS_NEXT_STEP]->"
                "(a:Step) RETURN DISTINCT a"
            )
            next_step = [Step.inflate(row[0]) for row in cypher_query_result2]
            next_step = sorted(next_step, key=step_sort)
            # insert a None element to avoid list index out of range
            next_step.append(None)

            cypher_query_result3, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step{step_id:"+repr(checklist_step)+"})-[:HAS_PREVIOUS_STEP]->"
                "(a:Step) RETURN DISTINCT a"
            )
            previous_step = [Step.inflate(row[0]) for row in cypher_query_result3]
            previous_step = sorted(previous_step, key=step_sort)
            # insert a None element to avoid list index out of range
            previous_step.insert(0, None)

            # TODo check if this can be removed -> use steps_list[0] instead
            # for step in steps_list:
            steps_list[0].next_step = next_step[0]
            steps_list[0].previous_step = previous_step[-1]

        elif (checklist_step == 'not_given' and (expertise_level in
              ['Expert', 'Intermediate', 'Novice'])):

            # expertise_level = expertise_level.capitalize()

            # a cypher query statement to retrieve data from the database
            cypher_query_result4, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(a:Step) WHERE a.type_always = ["+repr(expertise_level)+"] OR a.type_always = "
                "["+repr(expertise_level)+", 'Intermediate'] OR a.type_always = "
                "['Expert', "+repr(expertise_level)+"] OR a.type_always = "
                "['Expert', 'Intermediate', 'Novice'] RETURN DISTINCT a"
            )
            steps_list = [Step.inflate(row[0]) for row in cypher_query_result4]

            # a cypher query statement to retrieve data from the database - get all random steps
            cypher_query_result4, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(a:Step) WHERE a.type_random = ['Intermediate'] AND a.type_always = ['Novice']"
                " RETURN DISTINCT a"
            )
            all_random_novice_steps_list = [
                Step.inflate(row[0]) for row in cypher_query_result4
                # if expertise_level == 'Intermediate'
            ]
            random.seed(seed*5000)
            sel_random_steps = [
                random.sample(all_random_novice_steps_list, k=10)
                if expertise_level == 'Intermediate' else [] for i in range(1)
            ]
            sel_random_steps = sel_random_steps[0]
            steps_list.extend(sel_random_steps)
            steps_list = sorted(steps_list, key=step_sort)

            # this section provides additional functionality - not needed at the moment
            # add previous and next step fields for the response

            # for step in steps_list:
            #    # a cypher query statement to retrieve data from the database
            #    cypher_query_result5, meta = neomodel.db.cypher_query(
            #        "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
            #        "(:Step{step:"+repr(step.step)+"})-[:HAS_NEXT_STEP{expertise_level:"
            #        + repr(expertise_level)+"}]->(a:Step) RETURN DISTINCT a"
            #    )
            #    next_step = [Step.inflate(row[0]) for row in cypher_query_result5]
            #    # insert a None element to avoid list index out of range
            #    next_step.append(None)
            #
            #    cypher_query_result6, meta = neomodel.db.cypher_query(
            #        "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
            #        "(:Step{step:"+repr(step.step)+"})-[:HAS_PREVIOUS_STEP{expertise_level:"
            #        + repr(expertise_level)+"}]->(a:Step) RETURN DISTINCT a"
            #    )
            #    previous_step = [Step.inflate(row[0]) for row in cypher_query_result6]
            #    # insert a None element to avoid list index out of range
            #    previous_step.append(None)
            #
            #    step.next_step = next_step[0]
            #    step.previous_step = previous_step[0]

            first_step = steps_list[0]
            last_step = steps_list[-1]

            steps_list = [
                dict(item.__dict__, **{'first_step': first_step, 'last_step': last_step})
                for item in steps_list
            ]

        elif (checklist_step != 'not_given' and (expertise_level in
              ['Expert', 'Intermediate', 'Novice'])):

            # expertise_level = expertise_level.capitalize()

            # a cypher query statement to retrieve data from the database
            cypher_query_result7, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(a:Step{step_id:"+repr(checklist_step)+"}) WHERE a.type_always = ["
                + repr(expertise_level)+"] OR a.type_always ="
                " ["+repr(expertise_level)+", 'Intermediate'] OR a.type_always = "
                "['Expert', "+repr(expertise_level)+"] OR a.type_always = "
                "['Expert', 'Intermediate', 'Novice'] RETURN DISTINCT a"
            )
            # only one step in the steps_list
            steps_list = [Step.inflate(row[0]) for row in cypher_query_result7]

            # empty step list due to 2 reasons -> wrong step, or different user
            # check for the case for a different user with different expertise level
            if (len(steps_list) == 0):
                # a cypher query statement to retrieve data from the database
                cypher_query_result8, meta = neomodel.db.cypher_query(
                    "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                    "(a:Step{step_id:"+repr(checklist_step)+"}) RETURN DISTINCT a"
                )
                # only one step in the steps_list
                steps_list = [Step.inflate(row[0]) for row in cypher_query_result8]
                previous_expertise_step = steps_list[0].step

                # uncomment the below section for - changed previous step behaviour/
                # from previous user expertise
                # previous_expertise_step_id = steps_list[0].step_id
                # previous_expertise_list = ['Expert', 'Intermediate', 'Novice']
                # previous_expertise=previous_expertise_list[previous_expertise_step_id.count(".")]

                new_expertise = expertise_level

                # obtain the new step - new user expertise
                cypher_query_result9, meta = neomodel.db.cypher_query(
                    "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                    "(a:Step) WHERE a.type_always = ["
                    + repr(new_expertise)+"] OR a.type_always ="
                    " ["+repr(new_expertise)+", 'Intermediate'] OR a.type_always = "
                    "['Expert', "+repr(new_expertise)+"] OR a.type_always = "
                    "['Expert', 'Intermediate', 'Novice'] RETURN DISTINCT a"
                )
                new_exp_steps_list = \
                    [Step.inflate(row[0]) for row in cypher_query_result9]
                new_exp_steps_list = sorted(new_exp_steps_list, key=step_sort)
                next_step = [
                    new_exp_steps_list[i] for i in range(len(new_exp_steps_list))
                    if int(new_exp_steps_list[i].step) > int(previous_expertise_step)
                ]

                # Behaviour of previous step - derived from new user expertise,
                next_step_one = [
                    next_step[i].step_id if len(next_step) > 0
                    else checklist_step for i in range(1)
                ]

                # obtain the previous step - from new user expertise
                cypher_query_result10, meta = neomodel.db.cypher_query(
                    "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                    "(:Step{step_id:"+repr(next_step_one[0])+"})-[:HAS_PREVIOUS_STEP"
                    "]->(a:Step) RETURN DISTINCT a"
                )

                # insert a None element to avoid list index out of range
                next_step.append(None)

                # uncomment the below section for - changed previous step behaviour/
                # from previous user expertise
                """
                # obtain the previous step - from previous user expertise
                cypher_query_result10, meta = neomodel.db.cypher_query(
                    "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                    "(:Step{step_id:"+repr(checklist_step)+"})-[:HAS_PREVIOUS_STEP{expertise_level:"
                    + repr(previous_expertise)+"}]->(a:Step) RETURN DISTINCT a"
                )
                """
                previous_step = [Step.inflate(row[0]) for row in cypher_query_result10]
                previous_step = sorted(previous_step, key=step_sort, reverse=True)
                # insert a None element to avoid list index out of range
                previous_step.append(None)

            else:
                # a cypher query statement to retrieve data from the database
                cypher_query_result8, meta = neomodel.db.cypher_query(
                    "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                    "(:Step{step_id:"+repr(checklist_step)+"})-[:HAS_NEXT_STEP{expertise_level:"
                    + repr(expertise_level)+"}]->(a:Step) RETURN DISTINCT a"
                )
                next_step = [Step.inflate(row[0]) for row in cypher_query_result8]
                next_step = sorted(next_step, key=step_sort)
                # insert a None element to avoid list index out of range
                next_step.append(None)

                cypher_query_result9, meta = neomodel.db.cypher_query(
                    "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                    "(:Step{step_id:"+repr(checklist_step)+"})-[:HAS_PREVIOUS_STEP{expertise_level:"
                    + repr(expertise_level)+"}]->(a:Step) RETURN DISTINCT a"
                )
                previous_step = [Step.inflate(row[0]) for row in cypher_query_result9]
                previous_step = sorted(previous_step, key=step_sort, reverse=True)
                # insert a None element to avoid list index out of range
                previous_step.append(None)

            # TODo check if this can be removed -> use steps_list[0] instead,
            # for step in steps_list:
            steps_list[0].next_step = next_step[0]
            steps_list[0].previous_step = previous_step[0]

        elif (expertise_level not in
                ['Expert', 'Intermediate', 'Novice', 'Not_given']):

            # raise exception error
            raise GraphQLError(
                message="expertise_level: "+repr(expertise_level)+" is not valid"
            )

        # this may be removed since unnecessary here
        else:

            # raise exception error
            raise GraphQLError(
                message="Incorrect arguments format -> checklistSteps(checklist_id:"
                        + repr(checklist_id)+",checklist_step:"+repr(checklist_step) +
                        ",expertise_level:"+repr(expertise_level)+")"
            )

        # Randomly assigning steps to the intermediate level
        if (checklist_step != 'not_given' and expertise_level == 'Intermediate'):

            # a cypher query statement to retrieve data from the database
            cypher_query_result11, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(a:Step) WHERE a.type_random = ['Intermediate'] AND a.type_always = ['Novice']"
                " RETURN DISTINCT a"
            )

            all_random_novice_steps_list = [Step.inflate(row[0]) for row in cypher_query_result11]
            random.seed(seed*5000)
            sel_random_steps = random.sample(all_random_novice_steps_list, k=10)
            sel_random_steps = sorted(sel_random_steps, key=step_sort)

            next_step_ext = [
                sel_random_steps[i] for i in range(len(sel_random_steps))
                if int(sel_random_steps[i].step) > int(steps_list[0].step)
            ]

            prev_step_ext = [
                sel_random_steps[i] for i in range(len(sel_random_steps))
                if int(sel_random_steps[i].step) < int(steps_list[0].step)
            ]

            # removed the appended nontype form the next and previous steps
            next_step = list(filter(None, next_step))
            previous_step = list(filter(None, previous_step))

            # again sort the previous steps in ascending order(earlier in descending)
            previous_step = sorted(previous_step, key=step_sort)
            # next_setp already in ascending order

            next_step.extend(next_step_ext)
            next_step = sorted(next_step, key=step_sort)
            previous_step.extend(prev_step_ext)
            # we only need to extract
            # the closest smaller step to the current step
            previous_step = sorted(previous_step, key=step_sort)

            steps_list[0].next_step = next_step[0]
            steps_list[0].previous_step = previous_step[-1]  # closest step is the last(reversed)

        if (len(steps_list) == 0):

            # raise exception
            # raise Exception("no matching")
            raise GraphQLError(
                message="the chesklist step: "+repr(checklist_step) +
                        " or checklist id: "+repr(checklist_id) + "are not valid"
            )

        return steps_list

    @staticmethod
    def resolve_checklist_characteristics(
            root,
            info,
            checklist_id: Text,
            checklist_step: Text,
            checklist_language: Text,
            defect_id: Text,
            severity: Text) -> List[CharacteristicType]:
        """
        retrieves the characteristics for a checklist or a step in the checklist,
        alternatively retrieves the characteristics for a given defect_id and severity
        """

        if (checklist_step == 'not_given' and defect_id == 'not_given' and
                severity == 'not_given' and checklist_language in ['en', 'En']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result0, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step)-[:CONTAINS]->(:Defect)-[:HAS]->(a:Characteristic) RETURN DISTINCT a"
            )
            characteristics_list = [Characteristic.inflate(row[0]) for row in cypher_query_result0]
            characteristics_list = [
                {"description": item.description_en} for item in characteristics_list
            ]

        elif (checklist_step == 'not_given' and defect_id == 'not_given' and
                severity == 'not_given' and checklist_language in ['it', 'It']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result0, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step)-[:CONTAINS]->(:Defect)-[:HAS]->(a:Characteristic) RETURN DISTINCT a"
            )
            characteristics_list = [Characteristic.inflate(row[0]) for row in cypher_query_result0]
            characteristics_list = [
                {"description": item.description_it} for item in characteristics_list
            ]

        elif (checklist_step != 'not_given' and defect_id == 'not_given' and
                severity == 'not_given' and checklist_language in ['en', 'En']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result1, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step{step_id:"+repr(checklist_step)+"})-[:CONTAINS]->(:Defect)-[:HAS]->"
                "(a:Characteristic) RETURN DISTINCT a"
            )
            characteristics_list = [Characteristic.inflate(row[0]) for row in cypher_query_result1]
            characteristics_list = [
                {"description": item.description_en} for item in characteristics_list
            ]

        elif (checklist_step != 'not_given' and defect_id == 'not_given' and
                severity == 'not_given' and checklist_language in ['it', 'It']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result1, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step{step_id:"+repr(checklist_step)+"})-[:CONTAINS]->(:Defect)-[:HAS]->"
                "(a:Characteristic) RETURN DISTINCT a"
            )
            characteristics_list = [Characteristic.inflate(row[0]) for row in cypher_query_result1]
            characteristics_list = [
                {"description": item.description_it} for item in characteristics_list
            ]

        elif (checklist_step == 'not_given' and defect_id != 'not_given' and
                severity != 'not_given' and checklist_language in ['en', 'En']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result2, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step)-[:CONTAINS]->(:Defect{defect:"+repr(defect_id)+"})-"
                "[:HAS{severity: "+repr(severity)+"}]->(a:Characteristic) RETURN DISTINCT a"
            )
            characteristics_list = [Characteristic.inflate(row[0]) for row in cypher_query_result2]
            characteristics_list = [
                {"description": item.description_en} for item in characteristics_list
            ]

        elif (checklist_step == 'not_given' and defect_id != 'not_given' and
                severity != 'not_given' and checklist_language in ['it', 'It']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result2, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step)-[:CONTAINS]->(:Defect{defect:"+repr(defect_id)+"})-"
                "[:HAS{severity: "+repr(severity)+"}]->(a:Characteristic) RETURN DISTINCT a"
            )
            characteristics_list = [Characteristic.inflate(row[0]) for row in cypher_query_result2]
            characteristics_list = [
                {"description": item.description_it} for item in characteristics_list
            ]

        elif (checklist_step == 'not_given' and defect_id != 'not_given' and
                severity == 'not_given' and checklist_language in ['en', 'En']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result3, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step)-[:CONTAINS]->(:Defect{defect:"+repr(defect_id)+"})-"
                "[:HAS]->(a:Characteristic) RETURN DISTINCT a"
            )
            characteristics_list = [Characteristic.inflate(row[0]) for row in cypher_query_result3]
            characteristics_list = [
                {"description": item.description_en} for item in characteristics_list
            ]

        elif (checklist_step == 'not_given' and defect_id != 'not_given' and
                severity == 'not_given' and checklist_language in ['it', 'It']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result3, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step)-[:CONTAINS]->(:Defect{defect:"+repr(defect_id)+"})-"
                "[:HAS]->(a:Characteristic) RETURN DISTINCT a"
            )
            characteristics_list = [Characteristic.inflate(row[0]) for row in cypher_query_result3]
            characteristics_list = [
                {"description": item.description_it} for item in characteristics_list
            ]

        else:
            # raise exception error
            raise GraphQLError(
                message="Incorrect arguments format -> checklistCharacteristics(checklist_id:"
                + repr(checklist_id)+",checklist_step:"+repr(checklist_step)+",checklist_language:"
                + repr(checklist_language)+",defect_id:"
                + repr(defect_id)+",severity:"+repr(severity)+")"
            )

        if (len(characteristics_list) == 0):
            # raise exception
            # raise Exception("no matching")
            raise GraphQLError(message="no characteristics found")

        return characteristics_list

    @staticmethod
    def resolve_checklist_defects(
            root,
            info,
            checklist_id: Text,
            checklist_step: Text,
            component: Text,
            severity: Text,
            characteristic: Text) -> List[DefectType]:
        """
        retrieves the defects for a checklist or a step in the checklist,
        alternatively retrieves the defects for a given component,severity and characteristic
        """

        if (checklist_step == 'not_given' and component == 'not_given' and
                severity == 'not_given' and characteristic == 'not_given'):
            # a cypher query statement to retrieve data from the database
            cypher_query_result0, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step)-[:CONTAINS]->(a:Defect) RETURN DISTINCT a"
            )
            defects_list = [Defect.inflate(row[0]) for row in cypher_query_result0]

        elif (checklist_step != 'not_given' and component == 'not_given' and
                severity == 'not_given' and characteristic == 'not_given'):
            # a cypher query statement to retrieve data from the database
            cypher_query_result1, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step{step_id:"+repr(checklist_step)+"})-[:CONTAINS]->"
                "(a:Defect) RETURN DISTINCT a"
            )
            defects_list = [Defect.inflate(row[0]) for row in cypher_query_result1]

        elif (checklist_step == 'not_given' and component != 'not_given' and
                severity != 'not_given' and characteristic != 'not_given'):
            # a cypher query statement to retrieve data from the database
            cypher_query_result2, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step)-[:CONTAINS]->(a:Defect) "
                "WHERE (a:Defect)-[:HAS{severity: "+repr(severity)+"}]->"
                "(:Characteristic{description_en:"+repr(characteristic)+"}) AND (a)-[:INVOLVES]->"
                "(:Component{component_en:"+repr(component)+"}) RETURN DISTINCT a"
            )
            defects_list = [Defect.inflate(row[0]) for row in cypher_query_result2]

        elif (checklist_step == 'not_given' and component != 'not_given' and
                severity == 'not_given' and characteristic == 'not_given'):
            # a cypher query statement to retrieve data from the database
            cypher_query_result2, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step)-[:CONTAINS]->(a:Defect) "
                "WHERE (a:Defect)-[:HAS]->"
                "(:Characteristic) AND (a)-[:INVOLVES]->"
                "(:Component{component_en:"+repr(component)+"}) RETURN DISTINCT a"
            )
            defects_list = [Defect.inflate(row[0]) for row in cypher_query_result2]

        else:
            # raise exception error
            raise GraphQLError(
                message="Incorrect arguments format -> checklistDefects(checklist_id:"
                        + repr(checklist_id)+",checklist_step:"+repr(checklist_step) +
                        ",component:"+repr(component)+",severity:"+repr(severity)+",characteristic:"
                        + repr(characteristic)+")"
            )

        return defects_list

    @staticmethod
    def resolve_checklist_components(
            root,
            info,
            checklist_id: Text,
            checklist_step: Text,
            checklist_language: Text,
            defect_id: Text) -> List[ComponentType]:
        """
        retrieves the components for a checklist or a step in the checklist,
        alternatively retrieves the components for a given defect_id
        """

        if (checklist_step == 'not_given' and defect_id == 'not_given' and
                checklist_language in ['en', 'En']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result0, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step)-[:CONTAINS]->(:Defect)-[:INVOLVES]->(a:Component) RETURN DISTINCT a"
            )
            components_list = [Component.inflate(row[0]) for row in cypher_query_result0]
            components_list = [{"name": item.component_en} for item in components_list]

        elif (checklist_step == 'not_given' and defect_id == 'not_given' and
                checklist_language in ['it', 'It']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result0, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step)-[:CONTAINS]->(:Defect)-[:INVOLVES]->(a:Component) RETURN DISTINCT a"
            )
            components_list = [Component.inflate(row[0]) for row in cypher_query_result0]
            components_list = [{"name": item.component_it} for item in components_list]

        elif (checklist_step != 'not_given' and defect_id == 'not_given' and
                checklist_language in ['en', 'En']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result1, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step{step_id:"+repr(checklist_step)+"})-[:CONTAINS]->"
                "(:Defect)-[:INVOLVES]->(a:Component) RETURN DISTINCT a"
            )
            components_list = [Component.inflate(row[0]) for row in cypher_query_result1]
            components_list = [{"name": item.component_en} for item in components_list]

        elif (checklist_step != 'not_given' and defect_id == 'not_given' and
                checklist_language in ['it', 'It']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result1, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step{step_id:"+repr(checklist_step)+"})-[:CONTAINS]->"
                "(:Defect)-[:INVOLVES]->(a:Component) RETURN DISTINCT a"
            )
            components_list = [Component.inflate(row[0]) for row in cypher_query_result1]
            components_list = [{"name": item.component_it} for item in components_list]

        elif (checklist_step == 'not_given' and defect_id != 'not_given' and
                checklist_language in ['en', 'En']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result2, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step)-[:CONTAINS]->(:Defect{defect:"+repr(defect_id)+"})"
                "-[:INVOLVES]->(a:Component) RETURN DISTINCT a"
            )
            components_list = [Component.inflate(row[0]) for row in cypher_query_result2]
            components_list = [{"name": item.component_en} for item in components_list]

        elif (checklist_step == 'not_given' and defect_id != 'not_given' and
                checklist_language in ['it', 'It']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result2, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step)-[:CONTAINS]->(:Defect{defect:"+repr(defect_id)+"})"
                "-[:INVOLVES]->(a:Component) RETURN DISTINCT a"
            )
            components_list = [Component.inflate(row[0]) for row in cypher_query_result2]
            components_list = [{"name": item.component_it} for item in components_list]

        else:
            # raise exception error
            raise GraphQLError(
                message="Incorrect arguments format -> checklistComponents(checklist_id:"
                        + repr(checklist_id)+",checklist_step:"+repr(checklist_step) +
                        ",checklist_language:"+repr(checklist_language) +
                        ",defect_id:"+repr(defect_id)+")"
            )

        if (len(components_list) == 0):
            # raise exception
            # raise Exception("no matching")
            raise GraphQLError(message="no components found")

        return components_list

    @staticmethod
    def resolve_checklist_actions(
            root,
            info,
            checklist_id: Text,
            checklist_step: Text,
            checklist_language: Text) -> List[ActionType]:
        """
        retrieves the actions for a checklist or a step in the checklist
        """
        if (checklist_step == 'not_given' and checklist_language in ['en', 'En']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result0, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step)-[:CONTAINS]->(a:Action) RETURN DISTINCT a"
            )
            actions_list = [Action.inflate(row[0]) for row in cypher_query_result0]
            actions_list = [{"name": item.action_en} for item in actions_list]

        elif (checklist_step == 'not_given' and checklist_language in ['it', 'It']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result0, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step)-[:CONTAINS]->(a:Action) RETURN DISTINCT a"
            )
            actions_list = [Action.inflate(row[0]) for row in cypher_query_result0]
            actions_list = [{"name": item.action_it} for item in actions_list]

        elif (checklist_step != 'not_given' and checklist_language in ['en', 'En']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result1, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step{step_id:"+repr(checklist_step)+"})-[:CONTAINS]->"
                "(a:Action) RETURN DISTINCT a"
            )
            actions_list = [Action.inflate(row[0]) for row in cypher_query_result1]
            actions_list = [{"name": item.action_en} for item in actions_list]

        elif (checklist_step != 'not_given' and checklist_language in ['it', 'It']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result1, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step{step_id:"+repr(checklist_step)+"})-[:CONTAINS]->"
                "(a:Action) RETURN DISTINCT a"
            )
            actions_list = [Action.inflate(row[0]) for row in cypher_query_result1]
            actions_list = [{"name": item.action_it} for item in actions_list]

        else:
            # raise exception error
            raise GraphQLError(
                message="Incorrect arguments format -> checklistActions(checklist_id:"
                        + repr(checklist_id)+",checklist_step:"
                        + repr(checklist_step)+",checklist_language:"+repr(checklist_language)+")"
            )

        if (len(actions_list) == 0):
            # raise exception
            # raise Exception("no matching")
            raise GraphQLError(message="no Actions found")

        return actions_list

    @staticmethod
    def resolve_checklist_objectives(
            root,
            info,
            checklist_id: Text,
            checklist_step: Text,
            checklist_language: Text) -> List[ObjectiveType]:
        """
        retrieves the objectives for a checklist or a step in the checklist
        """
        if (checklist_step == 'not_given' and checklist_language in ['en', 'En']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result0, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step)-[:CONTAINS]->(a:Object) RETURN DISTINCT a"
            )
            objectives_list = [Objective.inflate(row[0]) for row in cypher_query_result0]
            objectives_list = [{"name": item.objective_en} for item in objectives_list]

        elif (checklist_step == 'not_given' and checklist_language in ['it', 'It']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result0, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step)-[:CONTAINS]->(a:Object) RETURN DISTINCT a"
            )
            objectives_list = [Objective.inflate(row[0]) for row in cypher_query_result0]
            objectives_list = [{"name": item.objective_it} for item in objectives_list]

        elif (checklist_step != 'not_given' and checklist_language in ['en', 'En']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result1, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step{step_id:"+repr(checklist_step)+"})-[:CONTAINS]->"
                "(a:Object) RETURN DISTINCT a"
            )
            objectives_list = [Objective.inflate(row[0]) for row in cypher_query_result1]
            objectives_list = [{"name": item.objective_en} for item in objectives_list]

        elif (checklist_step != 'not_given' and checklist_language in ['it', 'It']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result1, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step{step_id:"+repr(checklist_step)+"})-[:CONTAINS]->"
                "(a:Object) RETURN DISTINCT a"
            )
            objectives_list = [Objective.inflate(row[0]) for row in cypher_query_result1]
            objectives_list = [{"name": item.objective_it} for item in objectives_list]

        else:
            # raise exception error
            raise GraphQLError(
                message="Incorrect arguments format -> checklistObjectives(checklist_id:"
                        + repr(checklist_id)+",checklist_step:"+repr(checklist_step)
                        + ",checklist_language:"+repr(checklist_language)+")"
            )

        if (len(objectives_list) == 0):
            # raise exception
            # raise Exception("no matching")
            raise GraphQLError(message="no Objectives found")

        return objectives_list

    @staticmethod
    def resolve_checklist_verbs(
            root,
            info,
            checklist_id: Text,
            checklist_step: Text,
            checklist_language: Text) -> List[VerbType]:
        """
        retrieves the verbs for a checklist or a step in the checklist
        """
        if (checklist_step == 'not_given' and checklist_language in ['en', 'En']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result0, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step)-[:CONTAINS]->(a:Verb) RETURN DISTINCT a"
            )
            verbs_list = [Verb.inflate(row[0]) for row in cypher_query_result0]
            verbs_list = [{"name": item.verb_en} for item in verbs_list]

        elif (checklist_step == 'not_given' and checklist_language in ['it', 'It']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result0, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step)-[:CONTAINS]->(a:Verb) RETURN DISTINCT a"
            )
            verbs_list = [Verb.inflate(row[0]) for row in cypher_query_result0]
            verbs_list = [{"name": item.verb_it} for item in verbs_list]

        elif (checklist_step != 'not_given' and checklist_language in ['en', 'En']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result1, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step{step_id:"+repr(checklist_step)+"})-[:CONTAINS]->"
                "(a:Verb) RETURN DISTINCT a"
            )
            verbs_list = [Verb.inflate(row[0]) for row in cypher_query_result1]
            verbs_list = [{"name": item.verb_en} for item in verbs_list]

        elif (checklist_step != 'not_given' and checklist_language in ['it', 'It']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result1, meta = neomodel.db.cypher_query(
                "MATCH (:Checklist{checklist_id:"+repr(checklist_id)+"})-[:CONTAINS]->"
                "(:Step{step_id:"+repr(checklist_step)+"})-[:CONTAINS]->"
                "(a:Verb) RETURN DISTINCT a"
            )
            verbs_list = [Verb.inflate(row[0]) for row in cypher_query_result1]
            verbs_list = [{"name": item.verb_it} for item in verbs_list]
        else:
            # raise exception error
            raise GraphQLError(
                message="Incorrect arguments format -> checklistVerbs(checklist_id:"
                        + repr(checklist_id)+",checklist_step:"+repr(checklist_step)+")"
            )

        if (len(verbs_list) == 0):
            # raise exception
            # raise Exception("no matching")
            raise GraphQLError(message="no verbs found")

        return verbs_list

    @staticmethod
    def resolve_checklist_defect_details(
            root,
            info,
            checklist_id: Text,
            checklist_language: Text,
            defect_id: Text) -> DefectDetailsType:
        """retrieves severity of a defect  based on its defect id"""

        cypher_query_result0, meta = neomodel.db.cypher_query(
                "MATCH (a :Checklist{checklist_id:"+repr(checklist_id)+"}) RETURN a")
        checklists = [Checklist.inflate(row[0]) for row in cypher_query_result0]

        if (len(checklists) > 0):

            cypher_query_result1, meta = neomodel.db.cypher_query(
                "MATCH(a:Defect{defect:"+repr(defect_id)+"})-[r:HAS]-(b:Characteristic) RETURN r"
            )
            severity = [Severity.inflate(row[0]) for row in cypher_query_result1]
            # return severity[0]
        else:
            raise GraphQLError(
                message="checklist id is not correct, there is no checklist with id="
                + repr(checklist_id))
        if len(severity) == 0:
            raise GraphQLError(
                message="defect id is not correct, there is no defect with id="
                + repr(defect_id))

        if (checklist_language in ['en', 'En']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result2, meta = neomodel.db.cypher_query(
                "MATCH (b:Defect{defect:"+repr(defect_id)+"})"
                "-[:INVOLVES]->(a:Component) RETURN DISTINCT a"
            )
            components_list = [Component.inflate(row[0]) for row in cypher_query_result2]
            components_list = [{"name": item.component_en} for item in components_list]

            # a cypher query statement to retrieve data from the database
            cypher_query_result3, meta = neomodel.db.cypher_query(
                "MATCH (b:Defect{defect:"+repr(defect_id)+"})-"
                "[:HAS]->(a:Characteristic) RETURN DISTINCT a"
            )
            characteristics_list = [Characteristic.inflate(row[0]) for row in cypher_query_result3]
            characteristics_list = [
                {"description": item.description_en} for item in characteristics_list
            ]

        elif (checklist_language in ['it', 'It']):
            # a cypher query statement to retrieve data from the database
            cypher_query_result2, meta = neomodel.db.cypher_query(
                "MATCH (b:Defect{defect:"+repr(defect_id)+"})"
                "-[:INVOLVES]->(a:Component) RETURN DISTINCT a"
            )
            components_list = [Component.inflate(row[0]) for row in cypher_query_result2]
            components_list = [{"name": item.component_it} for item in components_list]

            # a cypher query statement to retrieve data from the database
            cypher_query_result3, meta = neomodel.db.cypher_query(
                "MATCH (b:Defect{defect:"+repr(defect_id)+"})-"
                "[:HAS]->(a:Characteristic) RETURN DISTINCT a"
            )
            characteristics_list = [Characteristic.inflate(row[0]) for row in cypher_query_result3]
            characteristics_list = [
                {"description": item.description_it} for item in characteristics_list
            ]
        else:
            raise GraphQLError(
                message="checklist language is not correct, language should be 'en' or 'it' ")

        defect_details = {
            "characteristic": characteristics_list[0]["description"],
            "component": components_list[0]["name"],
            "severity": severity[0].severity
        }

        return defect_details

    @staticmethod
    def resolve_product_profiles(
            root,
            info,
            product_id: Text) -> List[ProductProfileType]:
        """retrieves product profiles with status, scanned users, etc."""

        product_with_users = []
        if product_id == 'not_given':
            # a cypher query statement to retrieve product profiles from the database
            cypher_query_result0, meta = neomodel.db.cypher_query(
                "MATCH (a:ProductProfile) RETURN DISTINCT a"
            )
            product_list = [ProductProfile.inflate(row[0]) for row in cypher_query_result0]

            for product in product_list:
                # a cypher query statement to retrieve user profiles from the database
                # scanned users feature is removed
                '''
                cypher_query_result1, meta = neomodel.db.cypher_query(
                    "MATCH (a:ProductProfile {product_identifier:"
                    + repr(product.product_identifier)+"}) MATCH (b:UserProfile)-"
                    "[:HAS_SCANNED]->(a) RETURN DISTINCT b")
                scanned_users_list = [
                    UserProfile.inflate(row[0]) for row in cypher_query_result1]
                product.scanned_users = [
                    {"username": item.user_id, "level": item.user_expertise_level,
                     "language": item.user_language} for item in scanned_users_list]
                '''
                # a cypher query statement to retrieve product status from the database
                cypher_query_result2, meta = neomodel.db.cypher_query(
                    "MATCH (a:ProductProfile {product_identifier:"
                    + repr(product.product_identifier)+"}) MATCH (a)-[:HAS_STATUS]->"
                    "(b:Status) RETURN DISTINCT b"
                )
                product_status_list = [Status.inflate(row[0]) for row in cypher_query_result2]
                product.status = product_status_list[0].name

                # a cypher query statement to retrieve product defects from the database
                cypher_query_result3, meta = neomodel.db.cypher_query(
                    "MATCH (a:ProductProfile {product_identifier:"
                    + repr(product.product_identifier)+"}) MATCH (b:Defect)-[:INVOLVES_PRODUCT]->"
                    "(a) RETURN DISTINCT b"
                )
                product.defects = [Defect.inflate(row[0]) for row in cypher_query_result3]

                product_with_users.append(product)

        else:
            cypher_query_result0, meta = neomodel.db.cypher_query(
                "MATCH (a:ProductProfile {product_identifier:"
                + repr(product_id)+"}) RETURN DISTINCT a"
            )
            product_list = [ProductProfile.inflate(row[0]) for row in cypher_query_result0]

            if (len(product_list) == 0):
                # raise exception
                raise GraphQLError(
                    message="no products found under this product id:"+repr(product_id)
                )

            for product in product_list:
                # a cypher query statement to retrieve user profiles from the database
                # scanned users feature is removed
                '''
                cypher_query_result1, meta = neomodel.db.cypher_query(
                    "MATCH (a:ProductProfile {product_identifier:"
                    + repr(product_id)+"}) MATCH (b:UserProfile)-"
                    "[:HAS_SCANNED]->(a) RETURN DISTINCT b"
                )
                scanned_users_list = [
                    UserProfile.inflate(row[0]) for row in cypher_query_result1
                ]
                product.scanned_users = [
                    {"username": item.user_id, "level": item.user_expertise_level,
                     "language": item.user_language} for item in scanned_users_list
                ]
                '''
                # a cypher query statement to retrieve product status from the database
                cypher_query_result2, meta = neomodel.db.cypher_query(
                    "MATCH (a:ProductProfile {product_identifier:"
                    + repr(product_id)+"}) MATCH (a)-[:HAS_STATUS]->(b:Status)"
                    " RETURN DISTINCT b"
                )
                product_status_list = [Status.inflate(row[0]) for row in cypher_query_result2]
                product.status = product_status_list[0].name

                # a cypher query statement to retrieve product defects from the database
                cypher_query_result3, meta = neomodel.db.cypher_query(
                    "MATCH (a:ProductProfile {product_identifier:"
                    + repr(product_id)+"}) MATCH (b:Defect)-[:INVOLVES_PRODUCT]->"
                    "(a) RETURN DISTINCT b"
                )
                product.defects = [Defect.inflate(row[0]) for row in cypher_query_result3]

                product_with_users.append(product)

        return product_with_users


class Mutation(ObjectType):
    """
    A class to represent mutations in graphql schema

    mutations
        create_user_product_relation (str): mutation of a user-product relationship update
        delete_user_product_relation (str): mutation of a user-product relationship update
        update_product_status (str): mutation of a product status update
        create_defect_product_relation (str): mutation of a defect-product relationship update
        delete_defect_product_relation (str): mutation of a defect-product relationship update
        update_product_profile (List[ProductProfileType]): mutation of a product profile update

        ...

    """
    # Defines a query which creates a relationship between a product profile and a user profile
    # and returns a string of the relationship name
    create_user_product_relation = String(
        input=Argument(InputUserProductRel, required=True)
    )

    # Defines a query which deletes a relationship between a product profile and a user profile
    # and returns a string of the relationship name
    delete_user_product_relation = String(
        input=Argument(InputUserProductRel, required=True)
    )

    # Defines a query which updates the relationship between product profile and its status
    # and returns a string of the relationship name
    update_product_status = String(
        input=Argument(InputProductStatus, required=True)
    )

    # Defines a query which creates a relationship between a product profile and a DCS defect
    # and returns a string of the relationship name
    create_defect_product_relation = String(
        input=Argument(InputProductDefectRel, required=True)
    )

    # Defines a query which deletes a relationship between a product profile and a DCS defect
    # and returns a string of the relationship name
    delete_defect_product_relation = String(
        input=Argument(InputProductDefectRel, required=True)
    )

    # Defines a query which updates a product profile and returns the updated product profile
    update_product_profile = GList(
        ProductProfileType,
        input=Argument(InputProductProfile, required=True)
    )

    @staticmethod
    def resolve_create_user_product_relation(
            root,
            info,
            input: Dict[Text, Any]) -> Text:
        """
        creates a relationship between a user profile and a product profile,
        (this updates the batabase)
        """
        user_id = input.product_id
        product_id = input.product_id

        # to check if the user_id is valid
        # a cypher query statement to retrieve user profiles from the database
        cypher_query_result0, meta = neomodel.db.cypher_query(
            "MATCH (UserProfile {user_id:"
            + repr(user_id)+"}) RETURN DISTINCT UserProfile"
        )
        user_list = [UserProfile.inflate(row[0]) for row in cypher_query_result0]

        # to check if the product_id is valid
        # a cypher query statement to retrieve product profiles from the database
        cypher_query_result1, meta = neomodel.db.cypher_query(
            "MATCH (a:ProductProfile {product_identifier:"
            + repr(product_id)+"}) RETURN DISTINCT a"
        )
        product_list = [ProductProfile.inflate(row[0]) for row in cypher_query_result1]

        # execute the relationship creation only if both user_id and product_id are valid
        if (len(user_list) > 0) and (len(product_list) > 0):
            # to check if there is already existing relationship bewtween the user and product
            # a cypher query statement to retrieve product profiles from the database
            cypher_query_result2, meta = neomodel.db.cypher_query(
                "MATCH (a:UserProfile {user_id:"
                + repr(user_id)+"}) MATCH (b:ProductProfile{product_identifier:"
                + repr(product_id)+"}) MATCH(a)-[r:HAS_SCANNED]->(b) RETURN DISTINCT b"
            )
            # current_relationships holds an already existing relationship
            current_relationships = [ProductProfile.inflate(row[0]) for row in cypher_query_result2]

            # execute the new relationship creation only if there is no relationship
            if len(current_relationships) == 0:
                neomodel.db.cypher_query(
                    "MATCH (a:UserProfile {user_id:"
                    + repr(user_id)+"}) MATCH (b:ProductProfile"
                    "{product_identifier:"
                    + repr(product_id)+"}) CREATE(a)-[:HAS_SCANNED]->(b)"
                )

                return_val = "HAS_SCANNED"
            else:
                raise GraphQLError(message="Relationship already exists")

        elif (len(user_list) == 0) and (len(product_list) > 0):
            # return_val = "Error - this user_id is not found in the database"
            raise GraphQLError("this user_id is not found in the database")
        elif (len(product_list) == 0) and (len(user_list) > 0):
            # return_val = "Error - this product_id is not found in the database"
            raise GraphQLError(
                message="this product_id:"+repr(product_id)+" is not found in the database"
            )
        else:
            raise GraphQLError(
                message="please provide a valid user_id and "
                        "product_id (Both user_id:"+repr(user_id)+" and product_id:"
                        + repr(product_id)+" are not found in the database)"
            )

        return return_val

    @staticmethod
    def resolve_delete_user_product_relation(
            root,
            info,
            input: Dict[Text, Any]) -> Text:
        """
        deletes a relationship between a user profile and a product profile,
        (this updates the batabase)
        """
        user_id = input.product_id
        product_id = input.product_id

        # a cypher query statement to retrieve user profiles from the database
        cypher_query_result0, meta = neomodel.db.cypher_query(
            "MATCH (UserProfile {user_id:"
            + repr(user_id)+"}) RETURN DISTINCT UserProfile"
        )
        user_list = [UserProfile.inflate(row[0]) for row in cypher_query_result0]

        # a cypher query statement to retrieve product profiles from the database
        cypher_query_result1, meta = neomodel.db.cypher_query(
            "MATCH (a:ProductProfile {product_identifier:"
            + repr(product_id)+"}) RETURN DISTINCT a"
        )
        product_list = [ProductProfile.inflate(row[0]) for row in cypher_query_result1]

        # execute the relationship deletion only if both user_id and product_id are valid
        if (len(user_list) > 0) and (len(product_list) > 0):
            # a cypher query statement to create a new user-product relationship
            neomodel.db.cypher_query(
                "MATCH (a:UserProfile {user_id:"
                + repr(user_id)+"}) MATCH (b:ProductProfile"
                "{product_identifier:"
                + repr(product_id)+"}) MATCH(a)-[r:HAS_SCANNED]->(b) DELETE r")
            return_val = "HAS_SCANNED"

        elif (len(user_list) == 0) and (len(product_list) > 0):
            raise GraphQLError(
                message="this user_id:"+repr(user_id)+" is not found in the database"
            )

        elif (len(product_list) == 0) and (len(user_list) > 0):
            raise GraphQLError(
                message="this product_id:"+repr(product_id)+" is not found in the database"
            )

        else:
            raise GraphQLError(
                message="please provide a valid user_id and "
                        "product_id (Both user_id:"+repr(user_id)+" and product_id:"
                        + repr(product_id)+" are not found in the database)"
            )

        return return_val

    @staticmethod
    def resolve_update_product_status(
            root,
            info,
            input: Dict[Text, Any]) -> Text:
        """
        updates a requested product profile status
        (this updates the batabase)
        """
        product_id = input.product_id
        status = input.status

        # a cypher query statement to retrieve product profiles from the database
        cypher_query_result1, meta = neomodel.db.cypher_query(
            "MATCH (a:ProductProfile {product_identifier:"
            + repr(product_id)+"}) RETURN DISTINCT a"
        )
        product_list = [ProductProfile.inflate(row[0]) for row in cypher_query_result1]
        valid_status = ['registered', 'produced', 'launched', 'planned']

        if ((status) in (valid_status)) and (len(product_list) > 0):
            # a cypher query statement to delete the existing a product status
            neomodel.db.cypher_query(
                "MATCH (a: ProductProfile {product_identifier:"
                + repr(product_id)+"})-[r:HAS_STATUS]->() "
                "DELETE r ;"
            )
            # a cypher query statement to create a new product status
            neomodel.db.cypher_query(
                "MATCH (a: ProductProfile {product_identifier:"
                + repr(product_id)+"}) "
                "MATCH (s:Status {name: "+repr(status)+"}) "
                "CREATE (a)-[r:HAS_STATUS]->(s) ;"
            )
            return_val = "HAS_STATUS"

        elif ((status) not in (valid_status)) and (len(product_list) > 0):
            raise GraphQLError(message=" the status:"+repr(status)+" is not valid")

        elif (len(product_list) == 0) and ((status) in (valid_status)):
            raise GraphQLError(
                message="this product_id:"+repr(product_id)+" is not found in the database"
            )

        else:
            raise GraphQLError(
                message="please provide a valid status and "
                        "an avaliable product_id(Both status:"+repr(status)+" and product_id:"
                        + repr(product_id)+" are not found in the database)"
            )

        return return_val

    @staticmethod
    def resolve_create_defect_product_relation(
            root,
            info,
            input: Dict[Text, Any]) -> Text:
        """
        creates a relationship between a DCD defect and a product profile,
        (this updates the batabase)
        """
        defect_id = input.defect_id
        product_id = input.product_id

        # to check if the user_id is valid
        # a cypher query statement to retrieve user profiles from the database
        cypher_query_result0, meta = neomodel.db.cypher_query(
            "MATCH (a:Defect {defect:"
            + repr(defect_id)+"}) RETURN DISTINCT a"
        )
        defect_list = [Defect.inflate(row[0]) for row in cypher_query_result0]

        # to check if the product_id is valid
        # a cypher query statement to retrieve product profiles from the database
        cypher_query_result1, meta = neomodel.db.cypher_query(
            "MATCH (a:ProductProfile {product_identifier:"
            + repr(product_id)+"}) RETURN DISTINCT a"
        )
        product_list = [ProductProfile.inflate(row[0]) for row in cypher_query_result1]

        # execute the relationship creation only if both user_id and product_id are valid
        if (len(defect_list) > 0) and (len(product_list) > 0):
            # to check if there is already existing relationship bewtween the user and product
            # a cypher query statement to retrieve product profiles from the database
            cypher_query_result2, meta = neomodel.db.cypher_query(
                "MATCH (a:Defect {defect:"
                + repr(defect_id)+"}) MATCH (b:ProductProfile{product_identifier:"
                + repr(product_id)+"}) MATCH (a)-[r:INVOLVES_PRODUCT]->(b) RETURN DISTINCT b"
            )
            # current_relationships holds an already existing relationship
            current_relationships = [ProductProfile.inflate(row[0]) for row in cypher_query_result2]

            # execute the new relationship creation only if there is no relationship
            if len(current_relationships) == 0:
                neomodel.db.cypher_query(
                    "MATCH (a:Defect {defect:"
                    + repr(defect_id)+"}) MATCH (b:ProductProfile"
                    "{product_identifier:"
                    + repr(product_id)+"}) MERGE (a)-[:INVOLVES_PRODUCT]->(b)"
                )

                return_val = "INVOLVES_PRODUCT"
            else:
                raise GraphQLError(message="Relationship already exists")

        elif (len(defect_list) == 0) and (len(product_list) > 0):
            # return_val = "Error - this user_id is not found in the database"
            raise GraphQLError(message="this defect_id:"+repr(defect_id)+" is not valid")
        elif (len(product_list) == 0) and (len(defect_list) > 0):
            # return_val = "Error - this product_id is not found in the database"
            raise GraphQLError(message="this product_id"+repr(product_id)+" is not valid")
        else:
            raise GraphQLError(
                message="both defect_id:"+repr(defect_id)+" and product_id:"
                        + repr(product_id)+" are not valid"
            )

        return return_val

    @staticmethod
    def resolve_delete_defect_product_relation(
            root,
            info,
            input: Dict[Text, Any]) -> Text:
        """
        deletes a relationship between a DCS defect and a product profile,
        (this updates the batabase)
        """
        defect_id = input.defect_id
        product_id = input.product_id

        # a cypher query statement to retrieve user profiles from the database
        cypher_query_result0, meta = neomodel.db.cypher_query(
            "MATCH (a:Defect {defect:"
            + repr(defect_id)+"}) RETURN DISTINCT a"
        )
        defect_list = [Defect.inflate(row[0]) for row in cypher_query_result0]

        # a cypher query statement to retrieve product profiles from the database
        cypher_query_result1, meta = neomodel.db.cypher_query(
            "MATCH (a:ProductProfile {product_identifier:"
            + repr(product_id)+"}) RETURN DISTINCT a"
        )
        product_list = [ProductProfile.inflate(row[0]) for row in cypher_query_result1]

        # execute the relationship deletion only if both user_id and product_id are valid
        if (len(defect_list) > 0) and (len(product_list) > 0):

            # to check if there is already existing relationship bewtween the user and product
            # a cypher query statement to retrieve product profiles from the database
            cypher_query_result2, meta = neomodel.db.cypher_query(
                "MATCH (a:Defect {defect:"
                + repr(defect_id)+"}) MATCH (b:ProductProfile{product_identifier:"
                + repr(product_id)+"}) MATCH (a)-[r:INVOLVES_PRODUCT]->(b) RETURN DISTINCT b"
            )
            # current_relationships holds an already existing relationship
            current_relationships = [ProductProfile.inflate(row[0]) for row in cypher_query_result2]

            if (len(current_relationships) != 0):

                # a cypher query statement to delete a new user-product relationship
                neomodel.db.cypher_query(
                    "MATCH (a:Defect {defect:"
                    + repr(defect_id)+"}) MATCH (b:ProductProfile"
                    "{product_identifier:"
                    + repr(product_id)+"}) MATCH (a)-[r:INVOLVES_PRODUCT]->(b) DELETE r"
                )
                return_val = "INVOLVES_PRODUCT"
            else:
                raise GraphQLError(
                    message="no relationships exists between defect_id:"
                            + repr(defect_id)+" and product_id:"+repr(product_id)
                )

        elif (len(defect_list) == 0) and (len(product_list) > 0):
            raise GraphQLError(message="this defect_id:"+repr(defect_id)+" is not valid")

        elif (len(product_list) == 0) and (len(defect_list) > 0):
            raise GraphQLError(message="this product_id"+repr(product_id)+" is not valid")

        else:
            raise GraphQLError(
                message="both defect_id:"+repr(defect_id)+" and product_id:"
                        + repr(product_id)+" are not valid"
            )

        return return_val

    @staticmethod
    def resolve_update_product_profile(
            root,
            info,
            input: Dict[Text, Any]) -> List[ProductProfileType]:

        """
        updates a requested product profile details
        (this updates the batabase)
        """
        product_id = input.product_id
        checklist_is_active = input.checklist_is_active
        active_checklist_step = input.active_checklist_step
        checklist_is_completed = input.checklist_is_completed

        # a cypher query statement to retrieve product profiles from the database
        cypher_query_result0, meta = neomodel.db.cypher_query(
            "MATCH (a:ProductProfile{product_identifier:"
            + repr(product_id)+"}) RETURN a")
        mached_id = [ProductProfile.inflate(row[0]) for row in cypher_query_result0]
        if (len(mached_id) > 0):

            # a cypher query statement to write data into a product profile
            neomodel.db.cypher_query(
                 "MATCH (a:ProductProfile {product_identifier:"
                 + repr(product_id)+"}) SET a.checklist_is_active ="
                 + repr(checklist_is_active)+" SET a.active_checklist_step = ["
                 + repr(active_checklist_step)+"] SET a.checklist_is_completed ="
                 + repr(checklist_is_completed)+" "
            )

            product_list = Query.resolve_product_profiles(
                         root, info, product_id
            )

            return product_list
        else:
            raise GraphQLError(
                message="this product_id"+repr(product_id)+" is not found in the database"
            )


schema = Schema(query=Query, mutation=Mutation, auto_camelcase=True)
