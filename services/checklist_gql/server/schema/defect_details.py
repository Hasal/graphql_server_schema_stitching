# GraphQl module to build Defect details in the GraphQL schema

import graphene


class DefectDetailsType(graphene.ObjectType):
    """
    A class to represent the defect object type in graphql schema.

    Attributes
        defect (str): the DCS defect id
    """

    characteristic = graphene.String(required=False)
    component = graphene.String(required=False)
    severity = graphene.String(required=False)
