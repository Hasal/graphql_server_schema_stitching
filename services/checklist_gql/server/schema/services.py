# GraphQl module to build health of services in the GraphQL schema

import graphene


class ServicesType(graphene.ObjectType):
    """
    A class to represent the health services object type in graphql schema.

    Attributes
        name (str): name of the service
        status (str): service status
    """

    name = graphene.String(required=False)
    status = graphene.String(required=False)


class ServicesResponse(graphene.ObjectType):
    """
    A class to represent the Gitlab or RasaX services response in graphql schema.

    Attributes
        service_name (str): name of the service
        response (str): service status
        status (str): service status
    """

    service = graphene.Field(ServicesType, required=False)
    response = graphene.String(required=False)
