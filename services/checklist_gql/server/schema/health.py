# GraphQl module to build health of services in the GraphQL schema

import graphene

from server.schema.services import ServicesType


class HealthType(graphene.ObjectType):
    """
    A class to represent the Health object type in graphql schema.

    Attributes
        services (List): the available services
    """

    services = graphene.List(ServicesType, required=False)
