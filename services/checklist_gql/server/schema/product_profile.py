# GraphQl module to build Product Profile lists in the GraphQL schema

import graphene
import neomodel

from server.schema.defect import DefectType
# from server.schema.user_profile import UserProfileType


class ProductProfileType(graphene.ObjectType):
    """
    A class to represent the product profile object type in graphql schema.

    Attributes
        product_identifier (str): the id of the product
        product_identifier_type (str)
        checklist_id (str)
        checklist_is_active (bool)
        active_checklist_step (list)
        checklist_is_completed (bool)

    """
    product_identifier = graphene.String(required=False)
    product_identifier_type = graphene.String(required=False)
    checklist_id = graphene.String(required=False)
    checklist_is_active = graphene.Boolean(required=False)
    active_checklist_step = graphene.List(graphene.String, required=False)
    checklist_is_completed = graphene.Boolean(required=False)

    # additional inherited attributes from user profiles
    # scanned_users = graphene.List(UserProfileType, required=False)
    status = graphene.String(required=False)
    defects = graphene.List(DefectType, required=False)


class ProductProfile(neomodel.StructuredNode):
    """
    A class to represent the product profile nodes in the graph database.

    Attributes
        product_identifier (str): the id of the product
        product_identifier_type (str)
        checklist_id (str)
        checklist_is_active (bool)
        active_checklist_step (list)
        checklist_is_completed (bool)

    """
    product_identifier = neomodel.StringProperty(required=False)
    product_identifier_type = neomodel.StringProperty(required=False)
    checklist_id = neomodel.StringProperty(required=False)
    checklist_is_active = neomodel.BooleanProperty(required=False)
    active_checklist_step = neomodel.ArrayProperty(required=False)
    checklist_is_completed = neomodel.BooleanProperty(required=False)
