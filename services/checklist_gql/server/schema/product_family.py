
import graphene
import neomodel


class ProductFamilyType(graphene.ObjectType):
    """
    A class to represent the product falimy object type in graphql schema.

    Attributes
        name(str): the name of the product family

    """
    name = graphene.String(required=False)


class ProductFamily(neomodel.StructuredNode):
    """
    A class to represent the product falimy nodes in the graph database.

    Attributes
        name(str): the name of the product family

    """
    name = neomodel.StringProperty(required=False)
