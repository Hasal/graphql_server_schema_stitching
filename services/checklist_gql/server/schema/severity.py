# GraphQl module to build Severity lists in the GraphQL schema

import graphene
import neomodel


class SeverityType(graphene.ObjectType):
    """
    A class to represent the defect severity in graphql schema.

    Attributes
        severity (str): severity value of the defect
    """

    severity = graphene.String(required=False)


class Severity(neomodel.StructuredNode):
    """
    A class to represent the defect severity in the graph database.

    Attributes
        severity (str): severity value of the defect
    """

    severity = neomodel.StringProperty(required=False)
