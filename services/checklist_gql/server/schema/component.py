# GraphQl module to build Component lists in the GraphQL schema

import graphene
import neomodel


class ComponentType(graphene.ObjectType):
    """
    A class to represent the component object type in graphql schema.

    Attributes
        name (str): component name in English/Italian
    """

    name = graphene.String(required=False)


class Component(neomodel.StructuredNode):
    """
    A class to represent the Component nodes in the graph database.

    Attributes
        component_en (str): component name in English
        component_it (str): component name in Italian
    """

    component_en = neomodel.StringProperty(required=False)
    component_it = neomodel.StringProperty(required=False)
