# GraphQl module to build inputs types for mutations in the GraphQL schema

import graphene


class InputUserProductRel(graphene.InputObjectType):
    """
    A class to represent the input arg for user product relation mutations.

    Attributes
        user_id (str): the identity of the user
        product_id (str): the identity of the product

    """

    user_id = graphene.String(required=True)
    product_id = graphene.String(required=True)


class InputProductStatus(graphene.InputObjectType):
    """
    A class to represent the input arg for product status mutations.

    Attributes
        product_id (str): the identity of the product
        status (str): the name of the product status
    """

    product_id = graphene.String(required=True)
    status = graphene.String(required=True)


class InputProductDefectRel(graphene.InputObjectType):
    """
    A class to represent the input arg for product defect relation mutations.

    Attributes
        defect_id (str): the identity number of the defect
        product_id (str): the identity of the product
    """

    defect_id = graphene.String(required=True)
    product_id = graphene.String(required=True)


class InputUserProfile(graphene.InputObjectType):
    """
    A class to represent the input arg for user profile mutations.

    Attributes
        user_name (str): the name of user
        access_token(str): access token for the url
        data(dict): data to be written
    """

    user_name = graphene.String(required=True)
    access_token = graphene.String(required=True)
    data = graphene.JSONString(required=True)


class InputProductProfile(graphene.InputObjectType):
    """
    A class to represent the input arg for product profile mutations.

    Attributes
        product_id (str): the identity of the product
        checklist_is_active(bool): checklist activation status
        active_checklist_step(str): active step number of
        checklist_is_completed(bool): checklist completion status
    """

    product_id = graphene.String(required=True)
    checklist_is_active = graphene.Boolean(default_value=True)
    active_checklist_step = graphene.String(required=True)
    checklist_is_completed = graphene.Boolean(default_value=False)
