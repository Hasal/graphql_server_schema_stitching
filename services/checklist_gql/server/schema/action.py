# GraphQl module to build Action lists in the GraphQL schema

import graphene
import neomodel


class ActionType(graphene.ObjectType):
    """
    A class to represent the Action object type in graphql schema.

    Attributes
        name: the action that should be performed in English/Italian
    """

    name = graphene.String(required=False)


class Action(neomodel.StructuredNode):
    """
    A class to represent the Action nodes in the graph database.

    Attributes
        action_en (str): the action that should be performed in English
        action_it (str): the action that should be performed in Italian
    """

    action_en = neomodel.StringProperty(required=False)
    action_it = neomodel.StringProperty(required=False)
