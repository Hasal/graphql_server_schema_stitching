# GraphQl module to build user profile lists in the GraphQL schema
from graphene import ObjectType, String
# import neomodel


class UserProfile(ObjectType):
    """
    A class to represent the User profile object type in the graphql schema.

    Attributes
        username(str): the identity of the user
        expertise_level (str): the expertise level of the user
        language(str): the language of the user
    """

    user_name = String(required=True)
    expertise_level = String(required=False,)
    language = String(required=False)


# class UserProfile(neomodel.StructuredNode):
#     """
#     A class to represent the User profile nodes in the graph database.

#     Attributes
#         user_id(str): the identity of the user
#         user_expertise_level (str): the expertise level of the user
#         user_language(str): the language of the user
#     """

#     user_expertise_level = neomodel.StringProperty(required=False)
#     user_id = neomodel.StringProperty(required=False)
#     user_language = neomodel.StringProperty(required=False)
