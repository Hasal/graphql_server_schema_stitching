# GraphQl module to build Checklists in the GraphQL schema

import graphene
import neomodel

from server.schema.step import StepType


class ChecklistType(graphene.ObjectType):
    """
    A class to represent the checklist object type in graphql schema.

    Attributes
        checklist_id (str): the identity of the checklist
        expertise_level (str): the expertise level
        first_step (StepType): the 1st step  the checklist
        last_step (StepType): the final step of the checklist
    """

    checklist_id = graphene.String(required=False, name='id')
    expertise_level = graphene.String(required=False)
    first_step = graphene.Field(StepType, required=False)
    last_step = graphene.Field(StepType, required=False)


class Checklist(neomodel.StructuredNode):
    """
    A class to represent the checklist nodes in the graph database.

    Attributes
        checklist_id (str): the identity of the checklist
        first_step (str): the 1st step number of the checklist
        last_step (str): the final step number of the checklist
    """

    checklist_id = neomodel.StringProperty(required=False)
    first_step = neomodel.StringProperty(required=False)
    last_step = neomodel.StringProperty(required=False)
