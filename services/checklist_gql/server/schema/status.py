# GraphQl module to build Status lists in the GraphQL schema

import graphene
import neomodel


class StatusType(graphene.ObjectType):
    """
    A class to represent the Status of product in graphql schema.

    Attributes
        name(str): the name of the product family

    """
    name = graphene.String(required=False)


class Status(neomodel.StructuredNode):
    """
    A class to represent the Status of product in graph database.

    Attributes
        name(str): the name of the product status

    """
    name = neomodel.StringProperty(required=False)
