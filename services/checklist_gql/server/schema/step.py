# GraphQl module to build Step lists in the GraphQL schema

import graphene
import neomodel

from server.schema.temp_step import TempStepType


class StepType(graphene.ObjectType):
    """
    A class to represent the step object type in the graphql schema.

    Attributes
        step (str): the number of the step
        step_id(str): the identity of the step
        time(int): the time in seconds, requires for the execution of the step
        next_step(Field(dict)): the next step attributes
        previous_step(Field(dict)): the previous step attributes
        first_step(Field(dict)): the first step attributes
        last_step(Field(dict)): the last step attributes
    """

    step = graphene.String(required=False, name='stepNumber')
    step_id = graphene.String(required=False, name='id')
    time = graphene.Int(required=False)
    # add extra attributes to retrieve previous and next step
    next_step = graphene.Field(TempStepType, required=False)
    previous_step = graphene.Field(TempStepType, required=False)
    # add extra attributes to retrieve first and last step
    first_step = graphene.Field(TempStepType, required=False)
    last_step = graphene.Field(TempStepType, required=False)


class Step(neomodel.StructuredNode):
    """
    A class to represent the Step nodes in the graph database.

    Attributes
        step (str): the number of the step
        step_id(str): the identity of the step
        time(int): the time in seconds, requires for the execution of the step
    """

    step = neomodel.StringProperty(required=False)
    step_id = neomodel.StringProperty(required=False)
    time = neomodel.IntegerProperty(required=False)
