# GraphQl module to build Step lists in the GraphQL schema

import graphene


class TempStepType(graphene.ObjectType):
    """
    A class to represent the step object type in the graphql schema.

    Attributes
        step (str): the number of the step
        step_id(str): the identity of the step
        time(int): the time in seconds, requires for the execution of the step
    """

    step = graphene.String(required=False, name='stepNumber')
    step_id = graphene.String(required=False, name='id')
    time = graphene.Int(required=False)
