# GraphQl module to build User lists in the GraphQL schema

import graphene
import neomodel


class VerbType(graphene.ObjectType):
    """
    A class to represent the Verb object type in the graphql schema.

    Attributes
        verb (str): the verb for the step
    """

    name = graphene.String(required=False, name='name')


class Verb(neomodel.StructuredNode):
    """
    A class to represent the Verb nodes in the graph database.

    Attributes
        verb (str): the verb for the step
    """

    verb_en = neomodel.StringProperty(required=False)
    verb_it = neomodel.StringProperty(required=False)
