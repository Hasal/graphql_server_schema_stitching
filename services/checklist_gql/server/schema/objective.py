# GraphQl module to build Objective lists in the GraphQL schema

import graphene
import neomodel


class ObjectiveType(graphene.ObjectType):
    """
    A class to represent the objective object type in graphql schema.

    Attributes
        name (str): name of the objective in English/Italian
    """

    name = graphene.String(required=False)


class Objective(neomodel.StructuredNode):
    """
    A class to represent the Object (Objective) nodes in the graph database.

    Attributes
        objective_en (str): name of the objective in English
        objective_it (str): name of the objective in Italian
    """

    objective_en = neomodel.StringProperty(required=False)
    objective_it = neomodel.StringProperty(required=False)
