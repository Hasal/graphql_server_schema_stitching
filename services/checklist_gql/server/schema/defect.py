# GraphQl module to build Defect lists in the GraphQL schema

import graphene
import neomodel


class DefectType(graphene.ObjectType):
    """
    A class to represent the defect object type in graphql schema.

    Attributes
        defect (str): the DCS defect id
    """

    defect = graphene.String(required=False, name='id')


class Defect(neomodel.StructuredNode):
    """
    A class to represent the DCS Defect id in the graph database.

    Attributes
        defect (str): the DCS defect id
    """

    defect = neomodel.StringProperty(required=False)
