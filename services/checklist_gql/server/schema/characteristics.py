# GraphQl module to build Characteristic lists in the GraphQL schema

import graphene
import neomodel


class CharacteristicType(graphene.ObjectType):
    """
    A class to represent the Characteristic object type in graphql schema.

    Attributes
        description (str): the DCS defect description in English/Italian
    """

    description = graphene.String(required=False)


class Characteristic(neomodel.StructuredNode):
    """
    A class to represent the Characteristic nodes in the graph database.

    Attributes
        description_en (str): the DCS defect description in English
        description_it (str): the DCS defect description in Italian
    """

    description_en = neomodel.StringProperty(required=False)
    description_it = neomodel.StringProperty(required=False)
