# Readme

This is a graphql server project that serves to queries coming from a graphql client.

## References

### Tutorials for graphene-python

Structure for schemas and resolvers.
<https://docs.graphene-python.org/en/latest/quickstart/#an-example-in-graphene>

## Features

### Neo4j integration with Graphql-Graphene is done using **neomodel**

This implementation requires additional "Type" ObjectType classes for each planned type. For instance a User (object type) requires a UserType (object type).

### REST API integration

GraphQL can interact with REST APIs. This repository implements this for user profiles.
