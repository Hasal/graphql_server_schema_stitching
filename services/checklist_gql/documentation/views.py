from django.shortcuts import render


# Create your views here.
# request -> response
def get_document(request):
    """render the index.html GraphQl documentation for the current GraphQl schema"""
    return render(request, 'index.html')
