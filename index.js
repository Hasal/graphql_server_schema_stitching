const waitOn = require('wait-on');
const express = require('express');
const { graphqlHTTP } = require('express-graphql');
const { introspectSchema, RenameTypes, RenameRootFields } = require('@graphql-tools/wrap');
const { stitchSchemas } = require('@graphql-tools/stitch');
const { buildSchema } = require('graphql');

const makeRemoteExecutor = require('./lib/make_remote_executor');
//const localSchema = require('./services/local/schema');

// async function changed to func
async function makeGatewaySchema() {
  // Make remote executors:
  // these are simple functions that query a remote GraphQL API for JSON.
  const checklistExec = makeRemoteExecutor('http://checklist-gql-server:4001/graphql/'); // 0.0.0.0:4001/graphql  // http://localhost:4001/graphql/
  const auditExec = makeRemoteExecutor('http://audit-gql-server:4002/graphql');  // 0.0.0.0:4002/graphql
  const adminContext = { authHeader: 'Bearer my-app-to-app-token' };

  return stitchSchemas({
    subschemas: [
      {
        // 1. Introspect a remote schema. Simple, but there are caveats:
        // - Remote server must enable introspection.
        // - Custom directives are not included in introspection.
        schema: await introspectSchema(checklistExec, adminContext),
        executor: checklistExec,
      },
      {
        // 2. Manually fetch a remote SDL string, then build it into a simple schema.
        // - Use any strategy to load the SDL: query it via GraphQL, load it from a repo, etc.
        // - Allows the remote schema to include custom directives.
        schema: await introspectSchema(auditExec, adminContext),
        executor: auditExec,
      }

    ],
    // 3. Add additional schema directly into the gateway proxy layer.
    // Under the hood, `stitchSchemas` is a wrapper for `makeExecutableSchema`,
    // and accepts all of its same options. This allows extra type definitions
    // and resolvers to be added directly into the top-level gateway proxy schema.
    typeDefs: 'type Query { heartbeat: String! }',
    resolvers: {
      Query: {
        heartbeat: () => 'OK'
      }
    }
  });
}

// Custom fetcher that queries a remote schema for an "sdl" field.
// This is NOT a standard GraphQL convention – it's just a simple way
// for a remote API to provide its own schema, complete with custom directives.
async function fetchRemoteSDL(executor, context) {
  const result = await executor({ document: '{ _sdl }', context });
  return result.data._sdl;
}

// The root provides a resolver function for each API endpoint
var root = {
  hello: () => {
    return 'Hello world!';
  },
};

waitOn({ resources: ['http-get://audit-gql-server:4002/graphql'] }, async () => {
const schema = await makeGatewaySchema();
const app = express();
app.use('/graphql', graphqlHTTP ({
    schema: schema,
    rootValue: root,
    graphiql: true,
    //context: { authHeader: req.headers.authorization },
  }));
app.listen(4003);  //4003 seeems works without host='0.0.0.0'
console.log('Running a GraphQL API server at http://localhost:4003/graphql');

});
