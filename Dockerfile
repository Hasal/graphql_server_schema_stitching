FROM node:12-alpine

COPY ./lib lib
COPY ./index.js .
COPY ./package.json .
COPY ./yarn.lock .
RUN yarn install

RUN echo "fs.inotify.max_user_watches=524288" >> /etc/sysctl.conf

# ENV NODE_OPTIONS="--max-old-space-size=8192"

# EXPOSE 4003
CMD yarn start