## Schema-stitching
  #### 1. Combining remote schemas

  Here 2 remote schemas (each running in a seperate docker container) are stitched into a single schema.
  please follow the steps,
  1. edit the docker-compose.yaml file with the correct environment variables(uri, passwords etc)
  2. run the command ```docker-compose up --force-recreate --build -d ```

  #### for gitlab runner
  * for example: add these variables to the CI/CD Settings  
  ```
  $CHECKLIST_NEO4j_PASSWORD  
  $CHECKLIST_NEO4j_URI  
  $CHECKLIST_NEO4j_USERNAME  
  $AUDIT_NEO4j_PASSWORD  
  $AUDIT_NEO4j_URI  
  $AUDIT_NEO4j_USERNAME  
  ```

  Under build stage, put these scripts to the .gitlab-ci.yml  
  ```
  script:  
      - sed -i "s/CHECKLIST_NEO4j_USERNAME/$CHECKLIST_NEO4j_USERNAME/g" ./docker-compose.yml  
      - sed -i "s/CHECKLIST_NEO4j_URI/$CHECKLIST_NEO4j_URI/g" ./docker-compose.yml  
      - sed -i "s/CHECKLIST_NEO4j_PASSWORD/$CHECKLIST_NEO4j_PASSWORD/g" ./docker-compose.yml  
      - sed -i "s/AUDIT_NEO4j_USERNAME/$AUDIT_NEO4j_USERNAME/g" ./docker-compose.yml  
      - sed -i "s/AUDIT_NEO4j_URI/$AUDIT_NEO4j_URI/g" ./docker-compose.yml  
      - sed -i "s/AUDIT_NEO4j_PASSWORD/$AUDIT_NEO4j_PASSWORD/g" ./docker-compose.yml  
  ```
  Finally, the stitched GraphQL server with the Graphiql interface will be availabe on: http://localhost:4003/graphql

  #### 2. Combining local schemas
  will be tested at a later stage

  #### Additional: 
  * docker resource limits,
  Following configs can be set in the docker-compose.yaml file
  ```
  deploy:
      resources:
        limits:
          cpus: '0.2'
          memory: 512M
        reservations:
          cpus: '0.1'
          memory: 128M

  ```
